//
//  bottleViewController.swift
//  bottle animation
//
//  Created by Codetreasure on 14/01/20.
//  Copyright © 2020 Codetreasure. All rights reserved.
//

import UIKit


class bottleViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    let OptionsArr = ["Black1","White2","Brown3","Grape4","Orange5","purple6","pink7","blue8","grey9"]
    let OptionsImgArr = [UIImage(named: "bottle1"),UIImage(named: "bottle2"),UIImage(named: "bottle3"),UIImage(named: "bottle4"),UIImage(named: "bottle5"),UIImage(named: "bottle1"),UIImage(named: "bottle3"),UIImage(named: "bottle4"),UIImage(named: "bottle1")]
    @IBOutlet var collObj: UICollectionView!
    @IBOutlet var collObj2: UICollectionView!
  
    @IBOutlet var cartBtn: UIButton!
    @IBOutlet var yellowV: UIImageView!
    @IBOutlet var nameLb: UILabel!
    @IBOutlet var blackView: UIImageView!
    
    var sellectedIndex = IndexPath()
    override func viewDidLoad() {
        super.viewDidLoad()
        sellectedIndex = IndexPath(item: 0, section: 0)

        collObj.contentInset = UIEdgeInsets(top: 0, left: (view.bounds.width - 110) / 2, bottom: 0, right: (view.bounds.width - 110) / 2)

        collObj.decelerationRate = UIScrollView.DecelerationRate.fast
        
        collObj2.contentInset = UIEdgeInsets(top: 0, left: 45, bottom: 0, right: (view.bounds.width - 110) / 2)
        
        collObj2.decelerationRate = UIScrollView.DecelerationRate.fast
        
        blackView.layer.shadowColor = UIColor(red: 0.949, green: 0.773, blue: 0.024, alpha: 0.5).cgColor
        blackView.layer.shadowOpacity = 0.5
        blackView.layer.shadowOffset = CGSize(width: 0.0, height: 20.0)
        blackView.layer.shadowRadius = 20
        blackView.layer.masksToBounds = false
        cartBtn.layer.cornerRadius = 0.5 * cartBtn.bounds.size.width
        cartBtn.clipsToBounds = true

        
    }
    
    override func viewWillLayoutSubviews()
    {
        updateCellsLayout()
        super.viewWillLayoutSubviews()
    }
    
    
    func updateCellsLayout()
      {
          let centerX = collObj.contentOffset.x + collObj.frame.size.width/2
          
          for cell in collObj.visibleCells
          {
              var offsetX = centerX - cell.center.x
              if offsetX < 0
              {
                  offsetX *= -1
              }
              cell.transform = CGAffineTransform.identity
              let offsetPercentage = offsetX / (view.bounds.width * 1.7)
              let scaleX = 1-offsetPercentage
              cell.transform = CGAffineTransform(scaleX: scaleX, y: scaleX)
          }
        
        let centerX1 = collObj2.contentOffset.x + collObj2.frame.size.width/2
                 
                 for cell in collObj2.visibleCells
                 {
                     var offsetX = centerX1 - cell.center.x
                     if offsetX < 0
                     {
                         offsetX *= -1
                     }
                     cell.transform = CGAffineTransform.identity
                     let offsetPercentage = offsetX / (view.bounds.width * 1.7)
                     let scaleX = 1-offsetPercentage
                     cell.transform = CGAffineTransform(scaleX: scaleX, y: scaleX)
                 }

      }
    
  
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collObj{
        return OptionsArr.count
        }
        else {
            return OptionsArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collObj{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bottle cell", for: indexPath) as! CustumCollectionViewCell
        
//        cell.nameLB.text = OptionsArr[indexPath.row]  % OptionsArr.cout
        cell.imageview.image = OptionsImgArr[indexPath.row]
        
        return cell
        }
        else{
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "name cell", for: indexPath) as! CustumCollectionViewCell
            
            cell.nameLB.text = OptionsArr[indexPath.item]
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
      {
        if collectionView == collObj{
          return CGSize(width: 110 , height: 271)
        }
        else {
            return CGSize(width: 110, height: 110)
        }
      }
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
      {
        if collectionView == collObj{
            return (view.bounds.width - 110 * 2)/2 - 20
        }
        else {
            return (view.bounds.width - 110 * 2)/2 - 60
        }
         
      }
    
    //MARK: - collectionViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
       {
           updateCellsLayout()
        var visibleRect: CGRect = CGRect()
         visibleRect.origin = collObj.contentOffset
         visibleRect.size = collObj.bounds.size
         let visiblePoint:CGPoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        // let visibleIndexpath:IndexPath = collObj.indexPathForItem(at: visiblePoint)!

         if let visibleIndexpath = collObj.indexPathForItem(at: visiblePoint)
         {
            sellectedIndex = visibleIndexpath

         }
        UIView.animate(withDuration: 0.4) {

            self.collObj2.scrollToItem(at: self.sellectedIndex, at: [.left], animated: false)
        }
       
       }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell: CustumCollectionViewCell? = collObj.cellForItem(at: indexPath) as? CustumCollectionViewCell
        
        //collection cell co ordiantes
        let attributes: UICollectionViewLayoutAttributes? = collObj.layoutAttributesForItem(at: indexPath)
        let cellRect: CGRect? = attributes?.frame
        let cellFrameInSuperview: CGRect = collObj.convert(cellRect ?? CGRect.zero, to: collObj.superview?.superview)
        print("collection cell",cellFrameInSuperview)
        
        let storyB: UIStoryboard? = storyboard
        let obj = storyB?.instantiateViewController(withIdentifier: "bottleDetailViewController") as? bottleDetailViewController
        
        obj?.cardVX = Float(cellFrameInSuperview.origin.x)
        obj?.cardVY = Float(cellFrameInSuperview.origin.y)
        obj?.cardVWidth = Float(cellFrameInSuperview.size.width)
        obj?.cardVHeight = Float(cellFrameInSuperview.size.height)
        obj?.cardFrame = cell?.imageview.getFrameAsperWindow()//cellFrameInSuperview
        obj?.yellowVFrame = yellowV.getFrameAsperWindow()
        obj?.yellowVImage = UIImage(named: "yellow view") ?? UIImage()
        obj?.cardImage = OptionsImgArr[indexPath.row] ?? UIImage()
        obj?.animateIdentifier = 1  
        
        
        self.present(obj!, animated: false, completion: nil)

    }
    
}


