//
//  bottleDetailViewController.swift
//  bottle animation
//
//  Created by Codetreasure on 16/01/20.
//  Copyright © 2020 Codetreasure. All rights reserved.
//

import UIKit
import TTSegmentedControl

class bottleDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var cardVX:Float = 0.0
    var cardVY:Float = 0.0
    var cardVWidth:Float = 0.0
    var cardVHeight:Float = 0.0
    var animateIdentifier: Int = 0
    var cardFrame:CGRect!
    var yellowVFrame:CGRect!
    var cartBtnFrame:CGRect!
    var cardImage = UIImage()
    var yellowVImage = UIImage()
    var temp:Int = 0
    var timeCellClk: Int?
    var wavy = UIView()
    var noOfBottlesClicked:Bool = false
    var segment1Clicked:Bool = false
    var segment2Clicked:Bool = false

    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @IBOutlet var cardV: UIImageView!
    @IBOutlet var yellowV: UIImageView!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var tab: UITableView!
    @IBOutlet var rippleView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        temp = 0
        timeCellClk = 0
        
        cardV.isHidden = true
        yellowV.isHidden = true
        tab.alpha = 0
        rippleView.isHidden = true
        tab.estimatedRowHeight = 600

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cardV.isHidden = true
        yellowV.isHidden = true
        tab.alpha = 0
         self.animateOnEntry()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {super.viewDidAppear(animated)
        if animateIdentifier == 1
        {
            animateIdentifier = 0
            
            //---animation
            backBtn.alpha = 0

        }
    }
    
    @IBAction func backBtnClk(_ sender: Any)
       {
        self.animateOnExit()
       }

    
    func animateOnEntry(){
        
        cardV.isHidden = false
        yellowV.isHidden = false
        backBtn.alpha = 0
        let window = UIApplication.shared.keyWindow!
               window.addSubview(cardV)
               window.insertSubview(yellowV, belowSubview: cardV)
               // cardV
                cardV.frame = cardFrame
                yellowV.frame = yellowVFrame
                      
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
                   
                // cardV
            var rect: CGRect = self.cardV.frame
            rect.origin.x = 117.5//self.screenWidth/2
            rect.origin.y = 17//rect.origin.y - 100
            rect.size.width = 140
            rect.size.height = 200
            self.cardV.frame = rect
            
            // yellow view
            var rect1 :CGRect = self.yellowV.frame
            rect1.origin.y = -19
            self.yellowV.frame = rect1
                   
               }, completion: {(_ finished: Bool) -> Void in
                   if finished {

                    self.cardV.isHidden = true
                    self.yellowV.isHidden = true
                    self.tab.alpha = 1
                    //                       cell?.collObj.isHidden = true
                    //                       cell?.shareBtn.isHidden = true
                    
                    
                       self.reloadWithAnimationUp()
                   }
               })
    }
  func reloadWithAnimationUp() {
      
      let cells = tab.visibleCells
      var delayCounter = 0
    
      for cell in cells {

        let indexpath = tab.indexPath(for: cell)
        if indexpath != [0, 0] {
            cell.alpha = 0
            cell.transform = CGAffineTransform(translationX: 0, y: 60)
        }
      }
      
      for cell in cells {
        UIView.animate(withDuration: 0.4, delay: 0.05 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
              cell.transform = CGAffineTransform.identity

            cell.alpha = 1
          }, completion: nil)
          delayCounter += 1
      }
      
      let index = IndexPath.init(row: 0, section: 0)
      let cell1: customTableViewCell? = tab.cellForRow(at: index) as? customTableViewCell
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
        

//          cell1?.shareBtn.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
          UIView.animate(withDuration: 0.25, animations: {() -> Void in
//              cell1?.shareBtn.transform = .identity
              self.backBtn.alpha = 1

          })
      })
  }
    
    public func animateOnExit(){
        
        let index = IndexPath.init(row: 0, section: 0)
        let cell: bottleDetailViewController? = tab.cellForRow(at: index) as? bottleDetailViewController
        
        self.backBtn.alpha = 0
        self.tab.isHidden = true
        
        self.cardV.isHidden = false
        self.yellowV.isHidden = false
           
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
                
            // CardV
            var rect: CGRect = self.cardV.frame
            rect = self.cardFrame
            self.cardV.frame = rect
            
            // wavy view
            var rect1:CGRect = self.yellowV.frame
            rect1.origin.y = 0
            self.yellowV.frame = rect1
            
        
                
            }, completion: {(_ finished: Bool) -> Void in
                if finished {
                    
                    self.cardV.isHidden = false


                    self.dismiss(animated: false, completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.04, execute: {
                      
                        self.cardV.isHidden = true
                        self.yellowV.isHidden = true
                    })
                }
            })
            
        }
    
    func rippleAnimatation(){
    
            let window = UIApplication.shared.keyWindow!
            window.addSubview(rippleView)
            
            rippleView.frame = cartBtnFrame
            rippleView.layer.masksToBounds = false
            rippleView.layer.cornerRadius = rippleView.frame.height/2
            rippleView.clipsToBounds = true
            
            
           
            self.rippleView.isHidden = false
               
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
                    
             
            
                self.rippleView.transform = CGAffineTransform.identity.scaledBy(x: 25, y: 25)

                    
                }, completion: {(_ finished: Bool) -> Void in
                    if finished {
                        
                
                        UIView.animate(withDuration: 0.4, animations: {
                            self.rippleView.transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
                        }, completion: nil)
                  self.dismiss(animated: false, completion: nil)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.04, execute: {
                          
                        })
                    }
                })
    }

    //MARK: - UItableView DataSource
    
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:do {
            let cell = tableView.dequeueReusableCell(withIdentifier: "customTableViewCell", for: indexPath) as! customTableViewCell
            let image : UIImage = cardImage
            cell.imageview.image = image
            cardV.image = cardImage
            yellowV.image = yellowVImage
            yellowV.contentMode = .scaleToFill
            cell.selectionStyle = .none

            cell.arrowNameLb.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
            cell.arrowimageview.layer.shadowColor = UIColor(red: 0.949, green: 0.773, blue: 0.024, alpha: 0.5).cgColor
            cell.arrowimageview.layer.shadowOpacity = 0.5
            cell.arrowimageview.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
            cell.arrowimageview.layer.shadowRadius = 3.0
            cell.arrowimageview.layer.masksToBounds = false
            return cell
        }
        case 1:do {
            let cell = tableView.dequeueReusableCell(withIdentifier: "name Cell", for: indexPath) as! customTableViewCell
            cell.selectionStyle = .none
            
            return cell
            }
            case 2:do {
                       let cell = tableView.dequeueReusableCell(withIdentifier: "title cell", for: indexPath) as! customTableViewCell
                cell.selectionStyle = .none
                       
                       return cell
                       }
            case 3:do {
                       let cell = tableView.dequeueReusableCell(withIdentifier: "button cell", for: indexPath) as! customTableViewCell
                cell.selectionStyle = .none
                if noOfBottlesClicked == true {
                    cell.cartBtn.setImage(UIImage(named: "tick"), for: .normal)
                }
                cell.cartBtn.layer.cornerRadius = 0.5 * cell.cartBtn.bounds.size.width
                cell.cartBtn.clipsToBounds = true
                cartBtnFrame = cell.cartBtn.getFrameAsperWindow()
                cell.blackView.layer.shadowColor = UIColor(red: 0.949, green: 0.773, blue: 0.024, alpha: 0.5).cgColor
                cell.blackView.layer.shadowOpacity = 0.5
                cell.blackView.layer.shadowOffset = CGSize(width: 0.0, height: 20.0)
                cell.blackView.layer.shadowRadius = 20
                cell.blackView.layer.masksToBounds = false
                       return cell
                       }
        case 4:do {
            let cell = tableView.dequeueReusableCell(withIdentifier: "segment cell", for: indexPath) as! customTableViewCell
            cell.selectionStyle = .none
            cell.segmentControl.thumbColor = UIColor.white
            cell.segmentControl.itemTitles = ["Subscribe","One-time buy"]

            return cell
            }
            case 5:do {
                   let cell = tableView.dequeueReusableCell(withIdentifier: "week cell", for: indexPath) as! customTableViewCell
            cell.selectionStyle = .none
                if segment1Clicked == true {
                    cell.segmentView1.isHidden = false
                    cell.segmentView2.isHidden = true
                    cell.segment1Btn.setTitleColor(UIColor(red: 0.949, green: 0.773, blue: 0.024, alpha: 1), for: .normal)
                    cell.segment2Btn.setTitleColor(.white, for: .normal)
                }
                else {
                    cell.segmentView1.isHidden = true
                    cell.segment1Btn.setTitleColor(.white, for: .normal)
                }
                if segment2Clicked == true {
                    cell.segmentView2.isHidden = false
                    cell.segmentView1.isHidden = true
                    cell.segment2Btn.setTitleColor(UIColor(red: 0.949, green: 0.773, blue: 0.024, alpha: 1), for: .normal)
                    cell.segment1Btn.setTitleColor(.white, for: .normal)
                }
                else {
                    cell.segmentView2.isHidden = true
                    cell.segment2Btn.setTitleColor(.white, for: .normal)
                }
                   return cell
                   }
            case 6:do {
                   let cell = tableView.dequeueReusableCell(withIdentifier: "no of bottle cell", for: indexPath) as! customTableViewCell
            cell.selectionStyle = .none
                cell.noBottleBtn.layer.borderWidth = 1
                cell.noBottleBtn.layer.borderColor = UIColor.white.cgColor
                cell.noBottleBtn.layer.cornerRadius = 0.5 * cell.noBottleBtn.bounds.size.width
                cell.noBottleBtn.clipsToBounds = true
                
                cell.no2BottleBtn.layer.borderWidth = 1
                cell.no2BottleBtn.layer.borderColor = UIColor.white.cgColor
                cell.no2BottleBtn.layer.cornerRadius = 0.5 * cell.no2BottleBtn.bounds.size.width
                cell.no2BottleBtn.clipsToBounds = true
                
                cell.no3BottleBtn.layer.borderWidth = 1
                cell.no3BottleBtn.layer.borderColor = UIColor.white.cgColor
                cell.no3BottleBtn.layer.cornerRadius = 0.5 * cell.no3BottleBtn.bounds.size.width
                cell.no3BottleBtn.clipsToBounds = true
                   return cell
                   }
            
        default:break
            
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "name Cell", for: indexPath) as! customTableViewCell
        cell.selectionStyle = .none
        
              return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
               case 0:do {
                return 290//self.screenHeight/2
            }
        case 1:do{
            if(temp == 1)
            {
                return 0
            }else{
                return 150//self.screenHeight/6.67
            }
            }
        case 2:do{
            if(temp == 1)
            {
                return 0
            }else{
                return 117
            }
            }
        case 3:do{
                return 127
            }
        case 4: do{
            return 80 //100
            }
        case 5: do{
            return 75
            }
        case 6: do{
            return 70
            }
        default:
        break
        }
        return UITableView.automaticDimension
    }
    
    //MARK:UITableView Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    @IBAction func cartBtnClk(_ sender: Any)
    { if noOfBottlesClicked == false{
            
            if timeCellClk == 0
                       {//expand cell
                           temp = 1
                           timeCellClk = 1
                       }
                       else
                       {//collapse cell
                           temp = 0
                           timeCellClk = 0
                       }
                       
                       let contentOffset = tab.contentOffset
                       tab.reloadRows(at: [IndexPath(row: 3, section: 0)], with: UITableView.RowAnimation.fade)
                       tab.setContentOffset(contentOffset, animated: false)
        }
    else {
        rippleAnimatation()
        }
    }
    
    @IBAction func bottlesNoBtn(_ sender: Any)
    {
        noOfBottlesClicked = true
        let contentOffset = tab.contentOffset
        tab.reloadRows(at: [IndexPath(row: 3, section: 0)], with: UITableView.RowAnimation.fade)
        tab.setContentOffset(contentOffset, animated: false)
    }
    @IBAction func bottlesNo6Btn(_ sender: Any)
       {
           noOfBottlesClicked = true
           let contentOffset = tab.contentOffset
           tab.reloadRows(at: [IndexPath(row: 3, section: 0)], with: UITableView.RowAnimation.fade)
           tab.setContentOffset(contentOffset, animated: false)
       }
    @IBAction func bottlesNo9Btn(_ sender: Any)
       {
           noOfBottlesClicked = true
           let contentOffset = tab.contentOffset
           tab.reloadRows(at: [IndexPath(row: 3, section: 0)], with: UITableView.RowAnimation.fade)
           tab.setContentOffset(contentOffset, animated: false)
       }
    @IBAction func segment1Btn(_ sender: Any)
    {
        segment1Clicked = true
        segment2Clicked = false
        let contentOffset = tab.contentOffset
        tab.reloadRows(at: [IndexPath(row: 5, section: 0)], with: UITableView.RowAnimation.fade)
        tab.setContentOffset(contentOffset, animated: false)
        
    }
    @IBAction func segment2Btn(_ sender: Any)
       {
        segment2Clicked = true
        segment1Clicked = false
        let contentOffset = tab.contentOffset
        tab.reloadRows(at: [IndexPath(row: 5, section: 0)], with: UITableView.RowAnimation.fade)
        tab.setContentOffset(contentOffset, animated: false)
        
       }
}
