//
//  CustumCollectionViewCell.swift
//  bottle animation
//
//  Created by Codetreasure on 14/01/20.
//  Copyright © 2020 Codetreasure. All rights reserved.
//

import UIKit

class CustumCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageview: UIImageView!
    @IBOutlet var nameLB: UILabel!
}
