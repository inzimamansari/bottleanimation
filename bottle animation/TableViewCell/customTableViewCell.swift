//
//  customTableViewCell.swift
//  bottle animation
//
//  Created by Codetreasure on 17/01/20.
//  Copyright © 2020 Codetreasure. All rights reserved.
//

import UIKit
import TTSegmentedControl

class customTableViewCell: UITableViewCell {


    @IBOutlet var imageview: UIImageView!
    @IBOutlet var segmentControl: TTSegmentedControl!
    @IBOutlet var cartBtn: UIButton!
    @IBOutlet var noBottleBtn: UIButton!
    @IBOutlet var no2BottleBtn: UIButton!
    @IBOutlet var no3BottleBtn: UIButton!
    @IBOutlet var arrowNameLb: UILabel!
    @IBOutlet var arrowimageview: UIImageView!
    @IBOutlet var segmentView1:UIImageView!
    @IBOutlet var segmentView2:UIImageView!
    @IBOutlet var blackView:UIImageView!
    @IBOutlet var segment1Btn: UIButton!
    @IBOutlet var segment2Btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
